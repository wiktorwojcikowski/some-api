
import functools
import jwt
import settings
import base64

import tornado.web


def authenticated(method):
    
    @functools.wraps(method)
    def wrapper(self, *args, **kwargs):
        
        auth_header = self.request.headers.get('Authorization', '').strip()
        if not auth_header:
            raise tornado.web.HTTPError(403, 'Missing Authorizarion header')
        
        parts = auth_header.split()
        if len(parts) != 2 or parts[0].lower() != 'bearer':
            raise tornado.web.HTTPError(403, 
                'Wrong Authorization header format (Bearer token)')
        
        try:
            access_token = parts[1]
            decoded_token = jwt.decode(access_token, 
                base64.urlsafe_b64decode(settings.OAUTH_CLIENT_SECRET), 
                algorithms=[settings.OAUTH_ALGORITHM],
                audience=settings.OAUTH_AUDIENCE,
                issuer=settings.OAUTH_ISSUER)
        except jwt.exceptions.InvalidTokenError, e:
            raise tornado.web.HTTPError(403, log_message=str(e))

        self.access_token = access_token
        self.decoded_token = decoded_token
        return method(self, *args, **kwargs)
            
    return wrapper