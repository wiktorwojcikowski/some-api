import json
import tornado.web
import requests

import auth


class BaseHandler(tornado.web.RequestHandler):

    def write_json(self, obj):
      self.set_header('Content-Type', 'application/javascript')
      return self.write(json.dumps(obj))

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")


class HomeHandler(BaseHandler):

    def get(self):
        self.write_json({'text': 'this is api home'}) 


class ResourceHandler(BaseHandler):

    @auth.authenticated
    def get(self):
        self.write_json({'text': 'this is authenticated resource'}) 


class ProfileHandler(BaseHandler):

    @auth.authenticated
    def get(self):
        res = requests.get('https://samples.auth0.com/userinfo', 
            headers={'Authorization': 'Bearer %s' % self.access_token})

        self.write_json({'profile': res.text}) 

