Test oauth app
==============

Instalation
-----------
```
virtualenv .
source bin/activate
pip install -r requirements.txt
```

Run
---
```
python app.py
```

Test client
-----------
`http://localhost:8000/testclient/index.html`