# import settings
import tornado.ioloop
import tornado.web

import api.handlers

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/", api.handlers.HomeHandler),
            (r"/resource", api.handlers.ResourceHandler),
            (r"/profile", api.handlers.ProfileHandler),
            (r"/testclient/(.*)", tornado.web.StaticFileHandler, {"path": "testclient"}),
        ]
        tornado.web.Application.__init__(self, handlers)


app = Application()
if __name__ == "__main__":
    app.listen(8000)
    print 'server listen on port 8000' 
    tornado.ioloop.IOLoop.current().start()
